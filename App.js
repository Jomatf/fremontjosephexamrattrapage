import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import {useEffect, useState} from 'react';
import Form from './composant/Formulaire';

export default function App() {
  const [numberChoice, setNumberChoice] = useState(null);
  const [nbTentative, setNbTentative] = useState(0);
  const [message, setMessage] = useState("insérer un chiffre");
  const [number, setNumber] = useState(Math.floor(Math.random() * 11).toString());
  // const [response, setResponse] = useState(null);

  console.log('number ',number);
  
  const confirm = () => {
    setMessage(verif(numberChoice))
  }
  useEffect(() => {
    if(message == "ok"){
      setTimeout(()=> {
        setMessage("Vous pouvez rejouer");
        setNumber(Math.floor(Math.random() * 11).toString());
        setNbTentative(0);
      }, 5000)
    }
  },[message])

  let response;
  const verif = (nb) => {
    
    if(number==nb){
      response ="ok";
      setTimeout(()=> {
        response ="ok";
      }, 5000)
      
    }
    else if(nb > number){
      setNbTentative(nbTentative+1);
      response ="Le chiffre à trouvé est moins élevé";
    }
    else if(nb < number)
    {
      setNbTentative(nbTentative+1);
      response ="Le chiffre à trouvé est plus élevé";
    }
    console.log("response ",response);
    return response;
  }

  return (
    <View style={styles.container}>
      <View>
        {/* nom du jeu */}
        <Text style={{fontSize:30}}>NumberGame</Text>
      </View>
      <Form message={message} setMessage={setMessage}/>
      <Text style={[styles.box,{textAlign: 'center'}]}>
        {message}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  box: {
    width: '80%',
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor:'rgba(170,24,3, .4)',
    //ombre
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 0,
    margin:10,
    padding:10
  }
});
